const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const port = 3000


app.get('/', (req, res) => {
  res.send('Hello World!')
})

let bears = []
app.post('/api/bears',bodyParser.json(), (req, res) =>{
  bears.push(req.body)
  res.json({statusCode: 1, data : req.body})
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})